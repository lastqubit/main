﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GlobalNamespace;

public class CustomRPGScript : Photon.MonoBehaviour 
{

	public Transform alienStartPosition;
	public Transform scullyStartPosition;
	public Transform molderStartPosition;
	public Transform spectatorStartPosition;

    void OnJoinedRoom()
    {
        CreatePlayerObject();
    }

    void CreatePlayerObject()
    {
        if (photonView == null)
        {
            return;
        }
			
        if (photonView.ownerId != 1)
        {
			Vector3 position = alienStartPosition.position;
            GameObject newPlayerObject = PhotonNetwork.Instantiate("FPSController", position, Quaternion.identity, 0);
			GameStateController.Get.SetPlayerObject (newPlayerObject);
        }
    }
}
