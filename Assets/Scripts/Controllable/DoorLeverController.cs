﻿using UnityEngine;
using System.Collections.Generic;

public class DoorLeverController : MonoBehaviour
{
    private static DoorLeverController singleton;
    public static DoorLeverController Get()
    {
        return singleton;
    }

    public List <Lever> doorLeverObjects;

    void Awake()
    {
        singleton = this;
    }

    public void SendOpenData(Lever eventSource)
    {
        int index = doorLeverObjects.IndexOf(eventSource);
        byte[] doorLeverIndices = new byte[] { 1, (byte)(index & 255)};

        PhotonNetwork.RaiseEvent(CustomEventCode.DoorLeverSwitched, doorLeverIndices, true, null);
    }

    public void SendReleaseData(Lever eventSource)
    {
        int index = doorLeverObjects.IndexOf(eventSource);
        byte[] doorLeverIndices = new byte[] { 0, (byte)(index & 255) };

        PhotonNetwork.RaiseEvent(CustomEventCode.DoorLeverSwitched, doorLeverIndices, true, null);
    }

    public void SwitchLever(int index)
    {
        doorLeverObjects[index].PerformSwitch();
    }

    public void ReleaseLever(int index)
    {
        doorLeverObjects[index].OnTapReleased();
    }
}
