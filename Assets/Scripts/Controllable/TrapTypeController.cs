﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrapTypeController : MonoBehaviour
{
    public TrapType trapType;
    public List <TrapActivator> traps;
}
