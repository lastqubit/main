﻿using UnityEngine;
using System.Collections;
using System;

public class SpikeTrap : Controllable
{
    bool isAvailable = true;

    public override void OnReleaseEvent()
    {

    }

    public override void PerformControl()
    {
        if (!isAvailable)
            return;

        isAvailable = false;
        GetComponent<Animator>().SetTrigger("Activate");

        Invoke("PerformAttack", 2);
    }

    void PerformAttack()
    {
        GetComponent<Animator>().SetTrigger("Attack");
    }
}
