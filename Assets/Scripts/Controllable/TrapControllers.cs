﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrapControllers : MonoBehaviour
{
    private static TrapControllers singleton;
    public static TrapControllers Get()
    {
        return singleton;
    }
		
	public TrapTypeController []trapTypeController;

    Dictionary<TrapType, TrapTypeController> trapControllers;

    void Awake()
    {
        singleton = this;
        trapControllers = new Dictionary<TrapType, TrapTypeController>();

		for(int i = 0 ; i < trapTypeController.Length; ++i)
			
			trapControllers.Add (trapTypeController[i].trapType, trapTypeController[i]);
    }

    public void SendActivationData(TrapType trapType, TrapActivator trapActivator)
    {
        int index = 0;

        if (trapControllers.ContainsKey(trapType))
        {
            index = trapControllers[trapType].traps.IndexOf(trapActivator);
        }

        byte[] trapActivatorData = new byte[] { (byte)(((int)trapType) & 255), (byte)(index & 255) };

        PhotonNetwork.RaiseEvent(CustomEventCode.TrapActivated, trapActivatorData, true, null);
    }

    public void ActivateTrap(TrapType trapType, int trapActivatorIndex)
    {
        if(trapControllers.ContainsKey(trapType))
        {
            trapControllers[trapType].traps[trapActivatorIndex].PerformActivation();
        }
    }
}
