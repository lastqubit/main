﻿using UnityEngine;
using System.Collections;

public abstract class Controllable : MonoBehaviour
{
    public abstract void PerformControl();
    public abstract void OnReleaseEvent();
}
