﻿using UnityEngine;
using System.Collections;

public class FanTrap : Controllable {

	bool isAvailable = true;

	public override void OnReleaseEvent()
	{

	}

	public override void PerformControl()
	{
		if (!isAvailable)
			return;

		isAvailable = false;

		Invoke("PerformAttack", 0.5f);
	}

	void PerformAttack()
	{
		GetComponent<Animator>().SetTrigger("Attack");
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject == GameStateController.Get.GetPlayerobject ())
			GameStateController.Get.GetPlayerStats ().SubtractHp (20);
	}
}
