﻿using UnityEngine;

public class DoubleLockDoor : Door
{
    int pressedButtonsCounter = 0;

    Color red = new Color(1, 0, 0);
    Color green = new Color(0, 1, 0);

    public MeshRenderer[] doorLightsRenderers;

    public override void PerformControl()
    {
        pressedButtonsCounter++;

        if(doorLightsRenderers.Length >= pressedButtonsCounter)
        {
            if(doorLightsRenderers[pressedButtonsCounter - 1] != null)
            {
                doorLightsRenderers[pressedButtonsCounter - 1].material.color = green;
            }
        }

        if (!isOpened && pressedButtonsCounter >= 2)
        {
            isAvailable = false;
            GetComponent<Animator>().SetTrigger("Open");

            Invoke("ButtonTester", 1f);
        }
    }

    void ButtonTester()
    {
        if(pressedButtonsCounter < 2)
        {
            GetComponent<Animator>().SetTrigger("Close");
        }
    }

    public override void OnReleaseEvent()
    {
        if (pressedButtonsCounter > 0)
            pressedButtonsCounter--;

        if (doorLightsRenderers.Length >= pressedButtonsCounter)
        {
            if (doorLightsRenderers[pressedButtonsCounter] != null)
            {
                doorLightsRenderers[pressedButtonsCounter].material.color = red;
            }
        }
    }
}
