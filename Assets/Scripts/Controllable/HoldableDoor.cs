﻿using UnityEngine;
using System.Collections;

public class HoldableDoor : Door
{
    public override void PerformControl()
    {
        GetComponent<Animator>().SetTrigger("Open");
    }

    public override void OnReleaseEvent()
    {
        GetComponent<Animator>().SetTrigger("Close");
    }
}
