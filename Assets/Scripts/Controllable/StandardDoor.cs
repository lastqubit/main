﻿using UnityEngine;
using System.Collections;
using System;

public class StandardDoor : Door
{
    public override void PerformControl()
    {
        if (!isAvailable) // !Player.hasKey(lockColor);
            return;

        isAvailable = false;

        if (!isOpened)
            GetComponent<Animator>().SetTrigger("Open");
        else
            GetComponent<Animator>().SetTrigger("Close");

        Invoke("MakeAvailable", 6);
    }

    public override void OnReleaseEvent()
    {

    }

    void MakeAvailable()
    {
        isAvailable = true;
        isOpened = !isOpened;
    }
}
