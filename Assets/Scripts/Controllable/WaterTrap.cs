﻿using UnityEngine;
using System.Collections;

public class WaterTrap : Controllable {

	bool isAvailable = true;
	bool isActivated = false;

	float cooldown = 5.0f;
	float timeToTick = 5.0f;

	public GameObject electricSparks;

	public override void OnReleaseEvent()
	{

	}

	public override void PerformControl()
	{
		if (!isAvailable)
			return;

		isAvailable = false;

		Invoke("PerformAttack", 1f);
	}

	void PerformAttack()
	{
		//GetComponent<Animator>().SetTrigger("Attack");
		electricSparks.SetActive(true);
		isActivated = true;
	}

	void OnTriggerEnter(Collider other)
	{
		if (!isActivated)
			return;

		if (other.gameObject == GameStateController.Get.GetPlayerobject ())
			GameStateController.Get.GetPlayerStats ().SubtractHp (5);
	}

	void Update()
	{
		if (isActivated) {
			timeToTick -= Time.deltaTime;
			if (timeToTick < 0) {
				timeToTick = cooldown;
				isActivated = false;
				isAvailable = true;
				electricSparks.SetActive (false);
			}
		}
	}
}
