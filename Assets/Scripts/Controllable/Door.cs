﻿using UnityEngine;
using System.Collections;

public abstract class Door : Controllable
{
    protected bool isAvailable = true;
    protected bool isOpened = false;

    public abstract override void PerformControl();
    public abstract override void OnReleaseEvent();
}
