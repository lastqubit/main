﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectablesController : MonoBehaviour
{
	private static CollectablesController instance;

	static public CollectablesController Get { get { return instance; }  }

	public List <CollectableItem> colectableObjects;

	void Awake()
	{
		if (instance != null)
			Debug.LogWarning ("CollectablesController Singleton duplicated !");
		instance = this;
	}

	public void SendOpenData(CollectableItem eventSource)
	{
		int index = colectableObjects.IndexOf(eventSource);
		byte[] collectableItemIndices = new byte[] { (byte)(index & 255)};

		PhotonNetwork.RaiseEvent(CustomEventCode.ItemCollected, collectableItemIndices, true, null);
	}

	public void Collect(int index)
	{
		colectableObjects [index].PerformCollect ();
	}
}