﻿public enum TrapType
{
    SPIKE_TRAP = 0,
	FAN_TRAP = 1,
	CRT_TRAP = 2,
	ELECTRIC_WATER_TRAP = 3
}