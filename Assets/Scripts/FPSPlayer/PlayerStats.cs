﻿using UnityEngine;
using System.Collections;
using GlobalNamespace;
using System.Collections.Generic;

public class PlayerStats {

	Player playerType = Player.Unknown;
	int maxHp = 100;
	int myId = -1;
	int currentHp = 100;
	int currentStamina = 100;
	int maxStamina = 100;
	int score = 0;

	public delegate void PlayerStatisticsChange(int value);
	public static event PlayerStatisticsChange OnHpChange;
	public static event PlayerStatisticsChange OnStaminaChange;
	public static event PlayerStatisticsChange OnStaminaHitZero;
	public static event PlayerStatisticsChange OnScoreChange;
	public static event PlayerStatisticsChange OnInventoryChange;

	List<Collectable> inventory;

	public void AddHp(int amount)
	{
		currentHp = Mathf.Clamp (currentHp + amount, 0, maxHp);
		if (OnHpChange != null)
			OnHpChange (currentHp);
	}

	public void SubtractHp(int amount)
	{
		currentHp = Mathf.Clamp (currentHp - amount, 0, maxHp);
		if (OnHpChange != null)
			OnHpChange (currentHp);
		if (currentHp <= 0) {
			GameStateController.Get.SendGameStateData (100);
			//GameStateController.Get.GameLost ();
		}
	}

	public void AddScore( int amount )
	{
		score += amount;
		if (OnScoreChange != null)
			OnScoreChange (score);
	}

	public void SubtractScore( int amount )
	{
		score -= amount;
		if (OnScoreChange != null)
			OnScoreChange (score);
	}

	public void AddStamina( int amount )
	{
		currentStamina = Mathf.Clamp (currentStamina + amount, 0, maxStamina);
		if (OnStaminaChange != null)
			OnStaminaChange (currentStamina);
	}

	public void SubtractStamina( int amount )
	{
		currentStamina = Mathf.Clamp (currentStamina - amount, 0, maxStamina);
		if (OnStaminaChange != null)
			OnStaminaChange (currentStamina);
		if (currentStamina == 0 && OnStaminaHitZero != null) {
			OnStaminaHitZero (0);
		}
	}

	public void Reset()
	{
		inventory = new List<Collectable> ();
		AddStamina (maxStamina);
		AddHp (maxHp);
		if (playerType == Player.Alien)
			AddScore (10000);
		else
			AddScore (0);
	}

	public void SetPlayerType(Player type)
	{
		playerType = type;
	}

	public void SetMyId(int id)
	{
		myId = id;
	}

	public Player GetPlayerType()
	{
		return playerType;
	}

	public int GetScore()
	{
		return score;
	}

	public int GetHp()
	{
		return currentHp;
	}

	public int GetId()
	{
		return myId;
	}

	public int GetStamina()
	{
		return currentStamina;
	}

	public void AddToInventory(Collectable item )
	{
		inventory.Add (item);

		if( OnInventoryChange != null )
			OnInventoryChange (inventory.Count);
	}

	public void RemoveFromInventory( Collectable item )
	{
		if (inventory.Contains (item)) {
			inventory.Remove (item);
		}

		if( OnInventoryChange != null )
			OnInventoryChange (inventory.Count);
	}

	public List<Collectable> GetInventory()
	{
		return inventory;
	}

	public bool HasItem(Collectable item)
	{
		return inventory.Contains (item);
	}
}