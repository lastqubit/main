﻿using UnityEngine;
using System.Collections;

public class FirstPersonCameraNetwork :  Photon.MonoBehaviour {

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
		{
			//stream.SendNext((int)controllerScript._characterState);
			stream.SendNext(transform.rotation);
		}
		else
		{
			//Network player, receive data
			//controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();

			correctPlayerRot = (Quaternion)stream.ReceiveNext();
		}
	}

	private Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this

	void Update()
	{
		if (!photonView.isMine)
		{
			//Update remote player (smooth this, this looks good, at the cost of some accuracy)
			transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
		}
	}
}
