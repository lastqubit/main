﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using GlobalNamespace;

public class StaminaUsage : MonoBehaviour {

	bool staminaDrain = false;

	float staminaDrainTime = 0.1f;
	float currentTime = 0.0f;

	float walkingSpeed = 3.0f;
	float runningSpeed = 7.0f;
	float jumpSpeed = 5.0f;

	FirstPersonController firstPersonController;

	void OnEnable()
	{
		TimeManager.customUpdate += CustomUpdate;
		InputManager.OnKeyShiftUp += StopStainaDrain;
		InputManager.OnKeyShiftDown += StartStaminaDrain;
		InputManager.OnKeySpaceDown += PerformJump;
		PlayerStats.OnStaminaHitZero += NoStamina;
		GameStateController.OnPlayerSet += SetFirstPersonController;

	}

	void OnDisable()
	{
		TimeManager.customUpdate -= CustomUpdate;
		InputManager.OnKeyShiftUp -= StopStainaDrain;
		InputManager.OnKeyShiftDown -= StartStaminaDrain;
		InputManager.OnKeySpaceDown -= PerformJump;
		PlayerStats.OnStaminaHitZero -= NoStamina;
		GameStateController.OnPlayerSet -= SetFirstPersonController;
	}

	void CustomUpdate()
	{
		if (staminaDrain) {
			if (currentTime > staminaDrainTime) {
				GameStateController.Get.GetPlayerStats ().SubtractStamina (1);
				currentTime = 0;
			}
			currentTime += Time.deltaTime;
		} else {
			if (currentTime > staminaDrainTime*2) {
				GameStateController.Get.GetPlayerStats ().AddStamina (1);
				StaminaCheck ();
				currentTime = 0;
			}
			currentTime += Time.deltaTime;
		}
	}

	void StartStaminaDrain()
	{
		staminaDrain = true;
	}

	void StopStainaDrain()
	{
		staminaDrain = false;
	}

	void PerformJump()
	{
		if (GameStateController.Get.GetPlayerStats ().GetStamina () > 20 &&
			!firstPersonController.GetIsJumping() ) {
			firstPersonController.PerformJump (true);
			GameStateController.Get.GetPlayerStats ().SubtractStamina (20);
		}
	}

	void StaminaCheck()
	{
		if (firstPersonController == null) {
			SetFirstPersonController ();
			return;
		}
		if (GameStateController.Get.GetPlayerStats ().GetStamina () > 0) 
			firstPersonController.SetRunSpeed (runningSpeed);
	}

	void SetFirstPersonController()
	{
		if( GameStateController.Get.GetPlayerobject() != null )
		firstPersonController = GameStateController.Get.GetPlayerobject ().GetComponent<FirstPersonController> ();
		if (GameStateController.Get.GetCurrentPlayer () == Player.Alien)
			this.gameObject.SetActive (false);
	}

	void NoStamina(int stamina)
	{
		firstPersonController.SetRunSpeed (walkingSpeed);
	}
}
