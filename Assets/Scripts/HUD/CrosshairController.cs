﻿using UnityEngine;
using System.Collections;
using GlobalNamespace;

public class CrosshairController : MonoBehaviour {

	public Sprite activeCrosshair;
	public Sprite nonActiveCrosshair;

	public Sprite activePharaohCrosshair;
	public Sprite nonActivePharaohCrosshair;

	SpriteRenderer spriteRenderer;

	Player currentPlayer;
	bool playerSet = false;

	void RegisterCrosshair()
	{
		if (playerSet)
			return;
		if ((GameStateController.Get.GetCurrentPlayer() & Player.FPSPlayer) != 0) {
			InteractionRaycaster.OnRaycastEndInteraction += NonActiveCrosshair;
			InteractionRaycaster.OnRaycastStartInteraction += ActiveCrosshair;
		} else {
			PharaohInteractionRaycaster.OnRaycastEndInteraction += NonActiveCrosshair;
			PharaohInteractionRaycaster.OnRaycastStartInteraction += ActiveCrosshair;
		}
		currentPlayer = GameStateController.Get.GetCurrentPlayer ();
		playerSet = true;

		Debug.Log ("Register as : " + GameStateController.Get.GetCurrentPlayer ());
		if(  GameStateController.Get.GetCurrentPlayer () == Player.Spectator )
		CamerasController.OnObservedPlayerChanged += CrosshairSwitch;
	}

	void OnEnable()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
		SetCrosshairActive (false);
		GameStateController.OnPlayerSet += RegisterCrosshair;
	}

	public void SetCrosshairActive(bool isActive)
	{
		Player currentlyObserved = Player.Spectator;
		if (GameStateController.Get != null) {
			currentlyObserved = GameStateController.Get.cameraController.currentlyObserved;
			if (GameStateController.Get.GetCurrentPlayer () == Player.Spectator) {
				if ((currentlyObserved & Player.FPSPlayer) != 0)
					currentPlayer = Player.FPSPlayer;
				else
					currentPlayer = currentlyObserved;
			}
		}

		if (isActive) {
			if ((currentPlayer & Player.FPSPlayer) != 0) {
				spriteRenderer.sprite = activeCrosshair;
			} else {
				spriteRenderer.sprite = activePharaohCrosshair;
			}
		} else {
			if ((currentPlayer & Player.FPSPlayer) != 0) {
				spriteRenderer.sprite = nonActiveCrosshair;
			} else {
				spriteRenderer.sprite = nonActivePharaohCrosshair;
			}
		}

	}

	void ActiveCrosshair()
	{
		SetCrosshairActive (true);
	}

	void NonActiveCrosshair()
	{
		SetCrosshairActive (false);
	}

	void CrosshairSwitch()
	{	
		
		/*InteractionRaycaster.OnRaycastEndInteraction -= NonActiveCrosshair;
		InteractionRaycaster.OnRaycastStartInteraction -= ActiveCrosshair;
		PharaohInteractionRaycaster.OnRaycastEndInteraction -= NonActiveCrosshair;
		PharaohInteractionRaycaster.OnRaycastStartInteraction -= ActiveCrosshair;*/

		SetCrosshairActive (false);

		/*if ((currentlyObserved & Player.FPSPlayer) != null) {
			InteractionRaycaster.OnRaycastEndInteraction += NonActiveCrosshair;
			InteractionRaycaster.OnRaycastStartInteraction += ActiveCrosshair;
		}

		if (currentlyObserved == Player.Alien) {
			PharaohInteractionRaycaster.OnRaycastEndInteraction += NonActiveCrosshair;
			PharaohInteractionRaycaster.OnRaycastStartInteraction += ActiveCrosshair;
		}*/
	}
}
