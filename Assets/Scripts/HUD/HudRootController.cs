﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GlobalNamespace;
using UnityEngine.UI;

public class HudRootController : MonoBehaviour {

	public EnergyBar healthPoints;
	public EnergyBar progressBar;
	public EnergyBar staminaBar;

	public Text textMesh;
	public Text summaryText;

	public InventoryKeys inventoryKeys;


	void OnEnable()
	{
		HideProgressBar ();
		HideHealthBar ();
		HideStaminBar ();
		GameStateController.OnPlayerSet += OnPlayerSet;
		PlayerStats.OnHpChange += UpdateHpBar;
		PlayerStats.OnStaminaChange += UpdateStaminaBar;
		PlayerStats.OnScoreChange += UpdateScoreText;

	}

	void OnDisable()
	{
		GameStateController.OnPlayerSet -= OnPlayerSet;
		PlayerStats.OnHpChange -= UpdateHpBar;
		PlayerStats.OnStaminaChange -= UpdateStaminaBar;
		PlayerStats.OnScoreChange -= UpdateScoreText;
	}

	public void ShowStaminaBar ()
	{
		staminaBar.gameObject.SetActive (true);	
	}

	public void HideStaminBar()
	{
		staminaBar.gameObject.SetActive (false);
	}

	public void HideHealthBar()
	{
		healthPoints.gameObject.SetActive (false);
	}

	public void ShowHealthBar()
	{
		healthPoints.gameObject.SetActive (true);
	}

	public void HideProgressBar()
	{
		progressBar.gameObject.SetActive (false);
	}

	public void ShowProgressBar()
	{
		progressBar.gameObject.SetActive (true);
	}

	void OnPlayerSet()
	{
		if ((GameStateController.Get.GetCurrentPlayer () & Player.FPSPlayer) != 0) {
			ShowHealthBar ();
			ShowStaminaBar ();
			PlayerStats.OnInventoryChange += UpdateInventoryKeys;
		} else if ( GameStateController.Get.GetCurrentPlayer () == Player.Alien ||
					GameStateController.Get.GetCurrentPlayer () == Player.Spectator) {
			HideHealthBar ();

		}

		if( GameStateController.Get.GetCurrentPlayer () == Player.Spectator)
			CamerasController.OnObservedPlayerChanged += SwitchHud;

		//ShowProgressBar ();
		progressBar.SetValueMax (GameStateController.Get.totalScore);
	}

	void SwitchHud()
	{
		Player currentlyObserved = GameStateController.Get.cameraController.currentlyObserved;
		if ((currentlyObserved & Player.FPSPlayer) != 0) {
			ShowHealthBar ();
			ShowStaminaBar ();
		} else if ( currentlyObserved == Player.Alien) {
			HideHealthBar ();
			HideStaminBar ();
		} else if ( currentlyObserved == Player.Spectator)
		{
			HideHealthBar ();
			HideStaminBar ();
		}

	}
	public void UpdateHpBar(int value)
	{
		healthPoints.valueCurrent = value;
	}

	public void UpdateStaminaBar(int value)
	{
		staminaBar.valueCurrent = value;
	}

	public void UpdateScoreText(int value )
	{
		textMesh.text = value + " / " + GameStateController.Get.totalScore;
		progressBar.SetValueCurrent (value);
	}

	public void SetSummaryText(string text)
	{
		summaryText.text = text;
	}

	void UpdateInventoryKeys(int value)
	{
		inventoryKeys.ShowInventoryKeys (GameStateController.Get.GetPlayerStats ().GetInventory ());
	}
}
