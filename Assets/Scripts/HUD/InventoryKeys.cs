﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GlobalNamespace;

public class InventoryKeys : MonoBehaviour {

	public SpriteRenderer redKey;
	public SpriteRenderer greenKey;
	public SpriteRenderer blueKey;
	public SpriteRenderer yellowKey;

	List<SpriteRenderer> showedKeysList;

	void Start()
	{
		showedKeysList = new List<SpriteRenderer> ();
		showedKeysList.Add (redKey);
		showedKeysList.Add (greenKey);
		showedKeysList.Add (blueKey);
		showedKeysList.Add (yellowKey);

		ResetVisableKeys ();
	}

	public void ResetVisableKeys()
	{
		for (int i = 0; i < showedKeysList.Count; ++i) {
			showedKeysList [i].enabled = false;
		}
	}

	public void ShowInventoryKeys(List<Collectable> inventory )
	{
		ResetVisableKeys ();
		foreach (Collectable key in inventory) {
			if (key == Collectable.KeyRed)
				showedKeysList [0].enabled = true;
			else if (key == Collectable.KeyGreen)
				showedKeysList [1].enabled = true;
			else if (key == Collectable.KeyBlue)
				showedKeysList [2].enabled = true;
			else if (key == Collectable.KeyYellow)
				showedKeysList [3].enabled = true;
				
		}
	}


}
