﻿using UnityEngine;
using System.Collections;

public class EventCollector : MonoBehaviour
{
    void Awake()
    {
        PhotonNetwork.OnEventCall += this.OnEvent;
    }

    private void OnEvent(byte eventCode, object content, int senderid)
    {
        if(eventCode == CustomEventCode.DoorLeverSwitched)
        {
            byte[] leverIndices = (byte[])content;

            if(leverIndices[0] == 1)
            {
                DoorLeverController.Get().SwitchLever(leverIndices[1]);
            }
            else
            {
                DoorLeverController.Get().ReleaseLever(leverIndices[1]);
            }
        }

		if (eventCode == CustomEventCode.GameStateChanged) {
			byte[] gameState = (byte[])content;
			GameStateController.Get.DispatchGameState (gameState[0]);
		}

		if (eventCode == CustomEventCode.ItemCollected) {
			byte[] itemIndices = (byte[])content;

			foreach (byte itemID in itemIndices) {
				CollectablesController.Get.Collect (itemID);
			}
		}

        if (eventCode == CustomEventCode.TrapActivated)
        {
            byte[] trapActivatorData = (byte[])content;

            int trapType = trapActivatorData[0];
            int trapIndex = trapActivatorData[1];

            TrapControllers.Get().ActivateTrap((TrapType)trapType, trapIndex);
        }

		if (eventCode == CustomEventCode.AcidEvent) 
		{
			byte[] acidItemData = (byte[])content;
			if (acidItemData [0] == 1) {
				AcidBallController.Get.PerformAcidBallThrow ();
			} else if (acidItemData [0] == 2) {
				AcidBallController.Get.DestroyAcidBall ();
			}

		}
    }
}