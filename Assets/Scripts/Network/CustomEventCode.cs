﻿using UnityEngine;
using System.Collections;

public class CustomEventCode
{
    public const byte DoorLeverSwitched = 150;
    public const byte TrapActivated = 151;
	public const byte GameStateChanged = 152;
	public const byte ItemCollected = 153;
	public const byte AcidEvent = 154;

}
