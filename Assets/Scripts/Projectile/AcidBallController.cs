﻿using UnityEngine;
using System.Collections;
using GlobalNamespace;

public class AcidBallController : MonoBehaviour {

	private static AcidBallController instance;

	static public AcidBallController Get { get { return instance; }  }

	public GameObject acidBallPrefab;

	GameObject acidBall;


	void Awake()
	{
		if (instance != null)
			Debug.LogWarning ("AcidBallController Singleton duplicated !");
		instance = this;
	}

	void OnEnable()
	{
		GameStateController.OnPlayerSet += AcidBallAssignment;
	}

	void OnDisable()
	{
		InputManager.OnMouse0Up -= PerformAcidBallThrow;
	}
	

	public void SendOpenData(int msg)
	{
		byte[] acidItemMsgs = new byte[] { (byte)(msg & 255)};

		PhotonNetwork.RaiseEvent(CustomEventCode.AcidEvent, acidItemMsgs, true, null);
	}

	public void DestroyAcidBall()
	{
		acidBall.GetComponent<AcidBall>().PerformDestroyBall ();
	}

	public void PerformAcidBallThrow()
	{
		if (acidBall == null) {
			Quaternion rotation = GameStateController.Get.cameraController.alienCamera.gameObject.transform.rotation;
			Vector3 position = GameStateController.Get.cameraController.alienCamera.gameObject.transform.position;
			position += GameStateController.Get.cameraController.alienCamera.gameObject.transform.forward.normalized *2.0f;
			acidBall = (GameObject)Instantiate (acidBallPrefab, position, rotation);

		}

	}

	void AcidBallAssignment()
	{
		if (GameStateController.Get.GetCurrentPlayer() == Player.Alien)
			InputManager.OnMouse0Up += PerformAcidBallThrow;
	}
}      