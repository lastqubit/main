﻿using UnityEngine;
using System.Collections;
using GlobalNamespace;

public class AcidBall : Photon.MonoBehaviour {

	Rigidbody myRigidbody;
	float forceFactor = 150.0f;
	Vector3 startPosition;
	void OnEnable()
	{
		myRigidbody = GetComponentInChildren<Rigidbody> ();
		startPosition = this.transform.position;
		ThrowAcid (transform.forward.normalized);
	}

	void ThrowAcid(Vector3 pointingVector)
	{
		myRigidbody.AddForce (forceFactor * pointingVector,ForceMode.Impulse);
		if( GameStateController.Get.GetCurrentPlayer() == Player.Alien)
			AcidBallController.Get.SendOpenData (1);
		Debug.Log ("Adding Force :" + forceFactor * pointingVector);
	}

	void OnCollisionEnter(Collision collision)
	{
		Debug.Log ("Colliding with : " + collision.gameObject.name);
		if (collision.gameObject == GameStateController.Get.GetPlayerobject ())
			GameStateController.Get.GetPlayerStats ().SubtractHp (5);
		StartCoroutine ("DestroyBallOverTime");
	}

	public void PerformDestroyBall()
	{
		Destroy (this.gameObject);
	}

	IEnumerator DestroyBallOverTime()
	{
		yield return new WaitForSeconds (1f);

		Destroy (this.gameObject);
		yield return null;
	}

	void Update()
	{
		if (Vector3.Distance (transform.position, startPosition) > 50.0f)
			Destroy (this.gameObject);
	}
}