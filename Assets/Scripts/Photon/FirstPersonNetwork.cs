﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using GlobalNamespace;

public class FirstPersonNetwork : Photon.MonoBehaviour
{
    FirstPersonController controllerScript;

    void Awake()
    {
        if(photonView.ownerId == 1)
        {
			GameObject newPlayerObject = PhotonNetwork.Instantiate("AlienCamera",
				GameStateController.Get.customRPGScript.alienStartPosition.position , Quaternion.identity, 0);
			GameStateController.Get.SetPlayerObject (newPlayerObject);
			GameStateController.Get.SetPlayerType (Player.Alien);
			GameStateController.Get.SetPlayerId (1);
			if( GameStateController.Get != null )
				GameStateController.Get.cameraController.alienCamera = newPlayerObject.GetComponentInChildren<Camera> ();
			Debug.LogError ("Camera name = " + newPlayerObject.GetComponentInChildren<Camera> ().name);
			Debug.Log ("New player id = " + photonView.ownerId);
            PhotonNetwork.Destroy(this.gameObject);
            return;
        }

		if (photonView.ownerId == 4)
			GameStateController.Get.SetPlayerType (Player.Spectator);
		if( photonView.ownerId == 2 )
			GameStateController.Get.SetPlayerType (Player.Scully);
		if( photonView.ownerId == 3 )
			GameStateController.Get.SetPlayerType (Player.Mulder);
		
        controllerScript = GetComponent<FirstPersonController>();
		Debug.Log ("New player id = " + photonView.ownerId);

        if (photonView.isMine)
        {
            //MINE: local player, simply enable the local scripts
            MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
            for(int i = 0; i < renderers.Length; i++)
            {
                Destroy(renderers[i].gameObject);
            }

            controllerScript.enabled = true;
			if (photonView.ownerId == 2)
				this.gameObject.transform.position = GameStateController.Get.customRPGScript.scullyStartPosition.position;
			else if (photonView.ownerId == 3)
				this.gameObject.transform.position = GameStateController.Get.customRPGScript.molderStartPosition.position;
			else if (photonView.ownerId == 4) {
				this.gameObject.transform.position = GameStateController.Get.customRPGScript.spectatorStartPosition.position;
				controllerScript.mouseOnly = true;
				GameStateController.Get.cameraController.spectatorCamera = GetComponentInChildren<Camera> ();
			}
		}
        else
        {
			if (photonView.ownerId == 2) {
				GameStateController.Get.cameraController.scullyCamera = GetComponentInChildren<Camera> ();
			} else if (photonView.ownerId == 3) {
				GameStateController.Get.cameraController.mulderCamera = GetComponentInChildren<Camera> ();
			} 
			Destroy (GetComponentInChildren<InteractionSeeker> ());
			GameObject firstPersonCharacter = GetComponentInChildren<Camera> ().gameObject;
			firstPersonCharacter.GetComponent<Camera>().enabled = false;

            controllerScript.enabled = false;
            controllerScript.isControllable = false;
        }
		string name = "";
		if (photonView.ownerId == 2) {
			name = "Scully";
			foreach (Transform t in this.gameObject.transform)
				if (t.name == "Mulder")
					Destroy (t.gameObject);
		} else if (photonView.ownerId == 3) {
			name = "Molder";
			foreach (Transform t in this.gameObject.transform)
				if (t.name == "Scully")
					Destroy (t.gameObject);
		}
		else if (photonView.ownerId == 4)
			name = "Spectator";
        gameObject.name = name + photonView.viewID;
		GameStateController.Get.SetPlayerId (photonView.ownerId);
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //stream.SendNext((int)controllerScript._characterState);
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            //Network player, receive data
            //controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();
            correctPlayerPos = (Vector3)stream.ReceiveNext();
            correctPlayerRot = (Quaternion)stream.ReceiveNext();
        }
    }

    private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    private Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this

    void Update()
    {
        if (!photonView.isMine)
        {
            //Update remote player (smooth this, this looks good, at the cost of some accuracy)
            transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        }
    }
}
