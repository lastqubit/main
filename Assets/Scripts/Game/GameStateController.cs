﻿using UnityEngine;
using System.Collections;
using GlobalNamespace;

public class GameStateController : MonoBehaviour {

	private static GameStateController instance;
	static public GameStateController Get { get { return instance; }  }

	public CustomRPGScript customRPGScript;
	public HudRootController hudRootController;
	public CamerasController cameraController;

	public int totalScore = 10000;

	PlayerStats playerStats;

	public delegate void Game();
	public static event Game OnPlayerSet;
	public static event Game OnGameEnd;
	public static event Game OnGameStart;


	void Awake()
	{
		if (instance != null)
			Debug.LogWarning ("Singleton duplicated !");
		instance = this;
		playerStats = new PlayerStats();
	}
		
	void Start()
	{
		hudRootController.SetSummaryText ("");

		if (OnGameStart != null)
			OnGameStart ();
	}

	Player currentPlayer = Player.Unknown;
	public int playerId = -1;

	GameObject playerObject;

	public Player GetCurrentPlayer()
	{
		return playerStats.GetPlayerType ();
	}

	public void SetPlayerType( Player playerType)
	{
		
		if (playerStats.GetPlayerType () == Player.Unknown) {
			playerStats.SetPlayerType (playerType);
			currentPlayer = playerType;

			Debug.Log ("SetPlayer type = " + playerStats.GetPlayerType());
			if( (playerType & Player.FPSPlayer) != 0 )
			InputManager.OnKeyKUp += CheatKeyCollect;
			if (OnPlayerSet != null)
				OnPlayerSet ();
			playerStats.Reset ();
		}
	}

	public void SetPlayerId( int newPlayerId )
	{
		if (playerStats.GetId() == -1) {
			playerStats.SetMyId(newPlayerId);
			playerId = newPlayerId;
			Debug.LogError ("SetPlayerId = " + newPlayerId);
		}
	}

	public void SetPlayerObject(GameObject newPlayerObject)
	{
		if (playerObject == null) {
			playerObject = newPlayerObject;
			if( playerObject.GetComponentInChildren<AudioListener>() != null )
			playerObject.GetComponentInChildren<AudioListener> ().enabled = true;
		}
	}

	public GameObject GetPlayerobject()
	{
		return playerObject;
	}

	public PlayerStats GetPlayerStats()
	{
		return playerStats;
	}

	public void GameEnd()
	{
		if (OnGameEnd != null)
			OnGameEnd ();
	}

	public void GameWin()
	{
		hudRootController.SetSummaryText ("You Won ! \n " +
				"Score : " + playerStats.GetScore() + " / " +totalScore);
	}

	public void GameLost()
	{
		hudRootController.SetSummaryText ("You Lost ! \n " +
			"Score : " + playerStats.GetScore() + " / " +totalScore);
	}

	public void SendGameStateData(int gamestate)
	{
		Debug.LogWarning ("SendGameStateData :" + gamestate);
		Debug.LogWarning (" player : " + playerStats.GetPlayerType ());
		byte[] gameStateByte = new byte[] { (byte)(gamestate & 255)};

		PhotonNetwork.RaiseEvent(CustomEventCode.GameStateChanged, gameStateByte, true, null);
	}

	public void DispatchGameState(int gamestate)
	{
		Debug.LogWarning ("Dispatch Game State :" + gamestate);
		Debug.LogWarning (" player : " + playerStats.GetPlayerType ());
		/*if (gamestate == 100)
			PharaohWin ();
		else
			FPSplayerWin ();*/
	}

	void PharaohWin()
	{
		if (playerStats.GetPlayerType () == Player.Alien)
			GameWin ();
		else
			GameLost ();
	}

	void FPSplayerWin()
	{
		if ((playerStats.GetPlayerType () & Player.FPSPlayer) != 0)
			GameWin ();
		else
			GameLost ();
	}

	public void ItemCollect(CollectableItem collectableItem, Player playerType)
	{
		Debug.Log (collectableItem.type + " was collected by : " + playerType);
		playerStats.AddToInventory (collectableItem.type);
		/*if ((playerStats.GetPlayerType () & Player.FPSPlayer) != 0) {
			if (collectableItem.type == Collectable.HealthPotion)
				playerStats.AddHp (collectableItem.value);
			else if (collectableItem.type == Collectable.StaminaPotion)
				playerStats.AddStamina (collectableItem.value);
			else if (collectableItem.type == Collectable.Treasure)
				playerStats.AddScore (collectableItem.value);
		} else if (playerStats.GetPlayerType () == Player.Alien) {
			if (collectableItem.type == Collectable.Treasure)
				playerStats.SubtractScore (collectableItem.value);
		}*/
	}

	void CheatKeyCollect()
	{
		playerStats.AddToInventory (Collectable.KeyBlue);
		playerStats.AddToInventory (Collectable.KeyGreen);
		playerStats.AddToInventory (Collectable.KeyRed);
		playerStats.AddToInventory (Collectable.KeyYellow);
	}
}