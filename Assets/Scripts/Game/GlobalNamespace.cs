﻿namespace GlobalNamespace {
	public enum Player { 
		Alien = 0 << 1, 
		FPSPlayer = 1 << 1 , 
		Spectator = 2 << 1,
		Scully = 3 << 1 | FPSPlayer,
		Mulder = 4 << 1 | FPSPlayer,

		Unknown = 5 << 1 };
	
	public enum Collectable { Treasure, KeyRed, KeyGreen, KeyYellow, KeyBlue, HealthPotion, StaminaPotion , None}
};