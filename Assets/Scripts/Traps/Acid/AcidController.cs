﻿using UnityEngine;
using System.Collections;

public class AcidController : MonoBehaviour
{
    private static AcidController singleton;
    public static AcidController Get()
    {
        return singleton;
    }

    int acidCount = 0;

    float timerCoold = 0.5f;
    float timer = 0.5f;

    public delegate void OnUpdateAction();
    public event OnUpdateAction OnUpdate = null;

    public void IncreaseAcidCount()
    {
        acidCount++;

        if (acidCount > 0 && OnUpdate == null)
            OnUpdate += PerformDamage;
    }

    public void DecreaseAcidCount()
    {
        acidCount--;

        if(acidCount <= 0)
        {
            acidCount = 0;
            OnUpdate -= PerformDamage;
            OnUpdate = null;
        }
    }

    void PerformDamage()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            return;
        }

        GameStateController.Get.GetPlayerStats().SubtractHp(5);
        timer = timerCoold;
    }

    void Awake()
    {
        singleton = this;
    }

    void Update()
    {
        if (OnUpdate != null)
            OnUpdate();
    }
}
