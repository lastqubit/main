﻿using UnityEngine;
using System.Collections;

public class AcidPlane : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(GameStateController.Get.GetPlayerobject().name.Equals(other.gameObject.name))
            AcidController.Get().IncreaseAcidCount();
    }

    void OnTriggerExit(Collider other)
    {
        if (GameStateController.Get.GetPlayerobject() == other.gameObject)
            AcidController.Get().DecreaseAcidCount();
    }
}
