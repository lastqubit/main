﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour
{
	static TimeManager instance;

	public delegate void TimeListener();	
	public static event TimeListener customFixedUpdate;
	public static event TimeListener customUpdate;

	bool gamePaused;

	void Update()
	{
		if (!gamePaused) {
			if (customUpdate != null)
				customUpdate ();
		}
	}

	void FixedUpdate()
	{
		if (!gamePaused) {
			if (customFixedUpdate != null)
				customFixedUpdate();
		}
	}
		
	public void PauseGame()
	{
		gamePaused = true;
		Debug.Log ("Paused");
	}

	public void ResumeGame()
	{	
		gamePaused = false;
		Debug.Log ("Resumed");
	}

	public void Reset()
	{
		gamePaused = false;
	}
		
	void Awake()
	{
		if(instance == null)
			instance = new TimeManager();
	}
		
	static public TimeManager Get { get { return instance; }  }
}