﻿using System;
using UnityEngine;

public class ComputerScreen : Controllable
{
    public GameObject blueScreen;
    public GameObject windowsScreen;

    public override void OnReleaseEvent()
    {

    }

    public override void PerformControl()
    {
        blueScreen.SetActive(false);
        windowsScreen.SetActive(true);
    }
}
