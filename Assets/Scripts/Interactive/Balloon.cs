﻿using UnityEngine;
using System.Collections;
using System;

public class Balloon : Interactive
{
    public override void OnTapPerformed(GameObject tapSource)
    {
#if UNITY_ANDROID
        tapSource = tapSource.transform.parent.gameObject;
#endif

        tapSource.transform.parent = this.transform.parent;
        tapSource.transform.position = this.transform.position;
        tapSource.transform.rotation = this.transform.rotation;
    }

    public override void OnTapReleased()
    {

    }
}
