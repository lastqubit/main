﻿using UnityEngine;
using System.Collections;

public class PressurePlate : MonoBehaviour
{
    public Controllable controllableTarget;
    bool isPressable = true;

    void OnTriggerEnter(Collider other)
    {
        if (isPressable)
        {
            isPressable = false;
            GetComponent<AudioSource>().Play();

            if(controllableTarget != null)
                controllableTarget.PerformControl();

            GetComponent<Animator>().SetTrigger("Press");

            //DoorLeverController.Get().SendOpenData(this);
        }
    }

    public void PerformSwitch()
    {
        if (isPressable)
        {
            isPressable = false;

            if (controllableTarget != null)
                controllableTarget.PerformControl();

            GetComponent<Animator>().SetTrigger("Press");
        }
    }
}
