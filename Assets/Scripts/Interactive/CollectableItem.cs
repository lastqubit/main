﻿using UnityEngine;
using System.Collections;
using GlobalNamespace;

public class CollectableItem : MonoBehaviour {

	public Collectable type = Collectable.Treasure;
	public int value = 10;
	bool wasCollected = false;

	void OnTriggerEnter(Collider other) {
		CollectablesController.Get.SendOpenData (this);
		GameStateController.Get.ItemCollect (this, GameStateController.Get.GetCurrentPlayer());
		this.gameObject.SetActive (false);
	}

	public void PerformCollect()
	{
		//GameStateController.Get.ItemCollect (this);
		this.gameObject.SetActive (false);
	}
}
