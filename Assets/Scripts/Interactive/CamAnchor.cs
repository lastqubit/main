﻿using UnityEngine;
using System.Collections;
using System;

public class CamAnchor : Interactive
{
    GameObject tapSourceRef;
    Vector3 storedPos;
    Quaternion storedRot;

    public override void OnTapPerformed(GameObject tapSource)
    {
#if UNITY_ANDROID
        tapSource = tapSource.transform.parent.gameObject;
#endif

        tapSourceRef = tapSource;
        storedPos = tapSource.transform.position;
        storedRot = tapSource.transform.rotation;

        tapSource.transform.parent = this.transform.parent;
        tapSource.transform.position = this.transform.position;
        tapSource.transform.rotation = this.transform.rotation;

        InputManager.OnBackButtonDown += CamAnchorReleaser;
    }
    
    void CamAnchorReleaser()
    {
        tapSourceRef.transform.parent = null;
        tapSourceRef.transform.position = storedPos;
        tapSourceRef.transform.rotation = storedRot;

        InputManager.OnBackButtonDown -= CamAnchorReleaser;
    }

    public override void OnTapReleased()
    {

    }
}
