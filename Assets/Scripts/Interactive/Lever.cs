﻿using UnityEngine;
using System.Collections;
using System;
using GlobalNamespace;

public class Lever : Interactive
{
    public Controllable controllableTarget;
    bool isPressable = true;
    bool isPressed = false;

    public Collectable keyCode;

    public override void OnTapPerformed(GameObject tapSource)
    {
        if(isPressable && (keyCode == Collectable.None || GameStateController.Get.GetPlayerStats().HasItem(keyCode)))
        {
            isPressable = false;
            isPressed = true;
            controllableTarget.PerformControl();
            //GetComponent<Animator>().SetTrigger("Switch");

            DoorLeverController.Get().SendOpenData(this);
        }
    }

    public override void OnTapReleased()
    {
		return;
        isPressed = false;
        isPressable = true;
        controllableTarget.OnReleaseEvent();

        DoorLeverController.Get().SendReleaseData(this);
    }

    public void PerformSwitch()
    {
        controllableTarget.PerformControl();
        // GetComponent<Animator>().SetTrigger("Switch");
    }
}
