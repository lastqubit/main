﻿using UnityEngine;
using System.Collections;
using GlobalNamespace;

public class CamerasController : MonoBehaviour {

	public Camera mulderCamera;
	public Camera scullyCamera;
	public Camera alienCamera;
	public Camera spectatorCamera;

	public Player currentlyObserved = Player.Spectator;
	public delegate void Observer();
	public static event Observer OnObservedPlayerChanged;

	void OnEnable()
	{
		GameStateController.OnPlayerSet += OnPlayerSet;
	}

	void OnDisable()
	{
		GameStateController.OnPlayerSet -= OnPlayerSet;
		UnbindKeys ();
	}

	void OnPlayerSet()
	{
		if (GameStateController.Get.GetCurrentPlayer () == Player.Spectator) {
			InputManager.OnKey1Up += SetAlienCamera;
			InputManager.OnKey2Up += SetScullyCamera;
			InputManager.OnKey3Up += SetMulderCamera;
			InputManager.OnKey4Up += SetSpectatorCamera;
		}
	}

	void UnbindKeys()
	{
		if (GameStateController.Get.GetCurrentPlayer () == Player.Spectator) {
			InputManager.OnKey1Up -= SetAlienCamera;
			InputManager.OnKey2Up -= SetScullyCamera;
			InputManager.OnKey3Up -= SetMulderCamera;
			InputManager.OnKey4Up -= SetSpectatorCamera;
		}
	}

	void SetAlienCamera()
	{
		mulderCamera.enabled = false;
		scullyCamera.enabled = false;
		alienCamera.enabled = true;
		spectatorCamera.enabled = false;

		meshRenderers = scullyCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = true;

		meshRenderers = mulderCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = true;
		currentlyObserved = Player.Alien;

		if( OnObservedPlayerChanged != null )
		OnObservedPlayerChanged ();
	}

	void SetMulderCamera()
	{
		mulderCamera.enabled = true;
		scullyCamera.enabled = false;
		alienCamera.enabled = false;
		spectatorCamera.enabled = false;

		currentlyObserved = Player.Mulder;
		meshRenderers = scullyCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = true;
		
		meshRenderers = mulderCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = false;

		if( OnObservedPlayerChanged != null )
			OnObservedPlayerChanged ();
	}

	void SetScullyCamera()
	{
		mulderCamera.enabled = false;
		scullyCamera.enabled = true;
		alienCamera.enabled = false;
		spectatorCamera.enabled = false;

		currentlyObserved = Player.Scully;
		meshRenderers = scullyCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = false;

		meshRenderers = mulderCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = true;

		if( OnObservedPlayerChanged != null )
			OnObservedPlayerChanged ();
	}

	void SetSpectatorCamera()
	{
		mulderCamera.enabled = false;
		scullyCamera.enabled = false;
		alienCamera.enabled = false;
		spectatorCamera.enabled = true;

		currentlyObserved = Player.Spectator;

		meshRenderers = scullyCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = true;

		meshRenderers = mulderCamera.gameObject.transform.parent.GetComponentsInChildren<MeshRenderer> ();
		foreach(MeshRenderer mr in meshRenderers )
			mr.enabled = true;
		if( OnObservedPlayerChanged != null )
			OnObservedPlayerChanged ();
	}

	Component[] meshRenderers;
}