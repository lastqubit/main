﻿using UnityEngine;

public class TrapActivator : Interactive
{
    public Controllable controllableTarget;
    bool isPressable = true;

    public TrapType trapType;

    public override void OnTapPerformed(GameObject tapSource)
    {
        if (isPressable)
        {
            isPressable = false;
            controllableTarget.PerformControl();

            TrapControllers.Get().SendActivationData(trapType, this);
        }
    }

    public void PerformActivation()
    {
        if (isPressable)
        {
            isPressable = false;
            controllableTarget.PerformControl();
        }
    }

    public override void OnTapReleased()
    {

    }
}
