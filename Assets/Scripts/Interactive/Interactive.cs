﻿using UnityEngine;
using System.Collections;

public abstract class Interactive : MonoBehaviour
{
    public abstract void OnTapPerformed(GameObject tapSource);
    public abstract void OnTapReleased();
}
