﻿using UnityEngine;
using System.Collections;

public class DecorationActivator : Interactive
{
    public Controllable decorTarget;
    bool isPressable = true;
    bool isPressed = false;

    public override void OnTapPerformed(GameObject tapSource)
    {
        if (isPressable)
        {
            isPressable = false;
            isPressed = true;
            decorTarget.PerformControl();
            //GetComponent<Animator>().SetTrigger("Switch");
        }
    }

    public override void OnTapReleased()
    {
        if (isPressed)
        {
            isPressed = false;
            isPressable = true;
            decorTarget.OnReleaseEvent();
        }
    }

    void MakeAvailable()
    {
        isPressable = true;
    }
}
