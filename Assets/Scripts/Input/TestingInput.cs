﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections;

public class TestingInput : Photon.MonoBehaviour
{
    float speed = 1;
	MouseLook mouseLook;
	//MouseLook mouseLook;
	Camera camera;

    void Awake()
    {
		
        #if UNITY_ANDROID
			
            GameObject newParent = new GameObject();
            newParent.transform.position = this.transform.position;
            this.transform.parent = newParent.transform;
        #endif
    }

	void Start()
	{
		camera = GetComponentInChildren<Camera> ();
		mouseLook = new MouseLook ();
		mouseLook.Init(transform , camera.transform);
	}

	void FixedUpdate()
	{
		mouseLook.UpdateCursorLock();
	}

    void OnEnable()
    {
		#if UNITY_ANDROID
        OVRTouchpad.TouchHandler += HandleTouchHandler;
        Debug.Log("MSTANKI module attached");
		#endif
    }

    void OnDisable()
    {
		#if UNITY_ANDROID
        OVRTouchpad.TouchHandler -= HandleTouchHandler;
		Debug.Log("MSTANKI module detached");
		#endif
    }

	void Update ()
    {
		mouseLook.LookRotation (transform, camera.transform);
		/*if( mouseLook != null )
			mouseLook.LookRotation (transform, camera.transform);*/
        
		if (!photonView.isMine)
        {
			GetComponentInChildren<Camera>().enabled = false;
            Destroy(this);
            return;
        }


        if (Input.GetKey(KeyCode.D))
        {
            transform.localEulerAngles += new Vector3(0, speed, 0);
        }
        else if(Input.GetKey(KeyCode.A))
        {
            transform.localEulerAngles += new Vector3(0, -speed, 0);
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.localEulerAngles += new Vector3(speed, 0, 0);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.localEulerAngles += new Vector3(-speed, 0, 0);
        }
    }

    void HandleTouchHandler(object sender, System.EventArgs e)
    {
        OVRTouchpad.TouchArgs touchArgs = (OVRTouchpad.TouchArgs)e;

        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Up)
        {

        }

        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Down)
        {

        }

        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Left)
        {
            if(transform.parent.parent == null)
                transform.parent.Translate(transform.parent.forward);
        }

        if (touchArgs.TouchType == OVRTouchpad.TouchEvent.Right)
        {
            if (transform.parent.parent == null)
                transform.parent.Translate(-transform.parent.forward);
        }
    }
}
