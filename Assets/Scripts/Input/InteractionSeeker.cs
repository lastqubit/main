﻿using UnityEngine;
using System.Collections;

public class InteractionSeeker : MonoBehaviour
{
    Interactive interactiveTarget;
    Interactive storedInterTarget;

    bool isPressed = false;

	void Update ()
    {
	    if(InteractionRaycaster.Get().IsInteractivePresent(gameObject))
        {
            Interactive tempTarget = InteractionRaycaster.Get().GetRaycastTarget(gameObject);

            if(tempTarget != interactiveTarget)
            {
                if(interactiveTarget != null && isPressed)
                {
                    interactiveTarget.OnTapReleased();
                    isPressed = false;
                }

                interactiveTarget = InteractionRaycaster.Get().GetRaycastTarget(gameObject);
            }
        }
        else
        {
            if(interactiveTarget != null && isPressed)
            {
                interactiveTarget.OnTapReleased();
            }

            isPressed = false;
            interactiveTarget = null;
        }
        
        if(Input.GetMouseButtonDown(0))
        {
            storedInterTarget = interactiveTarget;

            if (interactiveTarget != null)
            {
                isPressed = true;
                interactiveTarget.OnTapPerformed(transform.parent.gameObject);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (storedInterTarget != null)
                storedInterTarget.OnTapReleased();

            isPressed = false;
            storedInterTarget = null;
        }
    }

	void OnTriggerEnter(Collider other) {
		if (other.name == "Finish") {
			GameStateController.Get.SendGameStateData (GameStateController.Get.GetPlayerStats ().GetId ());
		} 
	}
}
