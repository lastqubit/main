﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	private static InputManager instance;
	static public InputManager Get { get { return instance; }  }

	public delegate void OnKey ();
	public static event OnKey OnKeyShiftDown;
	public static event OnKey OnKeyShiftUp;

    public static event OnKey OnBackButtonDown;
	public static event OnKey OnKeySpaceDown;
	public static event OnKey OnKeySpaceUp;
	public static event OnKey OnKeySpace;

	public static event OnKey OnKeyKDown;

	public static event OnKey OnKey1Up;
	public static event OnKey OnKey2Up;
	public static event OnKey OnKey3Up;
	public static event OnKey OnKey4Up;

	public static event OnKey OnKeyKUp;
	public static event OnKey OnMouse0Up;


	void Awake()
	{
		if (instance != null)
			Debug.LogWarning ("Singleton duplicated !");
		instance = this;
	}

	void Update()
	{
		if (OnKeyShiftDown != null && (Input.GetKeyDown (KeyCode.LeftShift) || Input.GetKeyDown (KeyCode.RightShift)))
			OnKeyShiftDown ();
		
		if (OnKeyShiftUp != null && (Input.GetKeyUp (KeyCode.LeftShift) || Input.GetKeyUp (KeyCode.RightShift)))
			OnKeyShiftUp ();

		if(OnBackButtonDown != null && Input.GetKeyDown(KeyCode.Escape))
           OnBackButtonDown();

		if (OnKey1Up  != null && Input.GetKeyUp (KeyCode.Alpha1))
			OnKey1Up ();

		if (OnKey2Up  != null && Input.GetKeyUp (KeyCode.Alpha2))
			OnKey2Up ();

		if (OnKey3Up  != null && Input.GetKeyUp (KeyCode.Alpha3))
			OnKey3Up ();
		
		if (OnKey4Up  != null && Input.GetKeyUp (KeyCode.Alpha4))
			OnKey4Up ();

		if (OnMouse0Up != null && Input.GetMouseButtonUp (0))
			OnMouse0Up ();

		if (OnKeyKUp != null && Input.GetKey (KeyCode.K))
			OnKeyKUp ();
		
		if (OnKeySpace != null && Input.GetKey (KeyCode.Space))
			OnKeySpace ();
		
		if (OnKeySpaceDown != null && Input.GetKeyDown (KeyCode.Space))
			OnKeySpaceDown ();

		if (OnKeySpaceUp != null && Input.GetKeyUp (KeyCode.Space))
			OnKeySpaceUp ();

		if (OnKeyKDown != null && Input.GetKeyDown (KeyCode.K))
			OnKeyKDown ();
	}
}
