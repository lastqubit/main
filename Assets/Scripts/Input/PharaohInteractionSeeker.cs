﻿using UnityEngine;
using System.Collections;

public class PharaohInteractionSeeker : MonoBehaviour
{
    Interactive interactiveTarget;

    void Update()
    {
        if (PharaohInteractionRaycaster.Get().IsInteractivePresent(gameObject))
        {
            interactiveTarget = PharaohInteractionRaycaster.Get().GetRaycastTarget(gameObject);

            Color col = interactiveTarget.GetComponent<MeshRenderer>().material.color;
            col.a = 1;
            interactiveTarget.GetComponent<MeshRenderer>().material.color = col;
        }
        else
        {
            if(interactiveTarget != null)
            {
                Color col = interactiveTarget.GetComponent<MeshRenderer>().material.color;
                col.a = 0.5f;
                interactiveTarget.GetComponent<MeshRenderer>().material.color = col;
            }

            interactiveTarget = null;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (interactiveTarget != null)
                interactiveTarget.OnTapPerformed(transform.parent.gameObject);
        }
    }
}
