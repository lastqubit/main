﻿using UnityEngine;
using System.Collections;

public class InteractionRaycaster : MonoBehaviour
{
    private static InteractionRaycaster singleton;
    public static InteractionRaycaster Get()
    {
        return singleton;
    }

	public delegate void Raycast();
	public static event Raycast OnRaycastStartInteraction;
	public static event Raycast OnRaycastEndInteraction;
    public LayerMask mask;

    void Awake()
    {
        singleton = this;
    }

    public bool IsInteractivePresent(GameObject raycastSource)
    {
        Ray ray = new Ray(raycastSource.transform.position, raycastSource.transform.forward);

        if (Physics.Raycast(ray, 2f, mask))
        {
			if (OnRaycastStartInteraction != null)
				OnRaycastStartInteraction();
            return true;
        }	

		if (OnRaycastEndInteraction != null)
			OnRaycastEndInteraction();
        return false;
    }

    public Interactive GetRaycastTarget(GameObject raycastSource)
    {
        Ray ray = new Ray(raycastSource.transform.position, raycastSource.transform.forward);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 2f, mask))
        {
            return hit.collider.gameObject.GetComponent<Interactive>();
        }

        return null;
    }
}
